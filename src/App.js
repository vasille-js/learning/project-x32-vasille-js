import { MapX1 } from "./components/x1/MapX1";
import { BetonBlock, BlockPack, BrickBlock } from "./types/block";
import { AppNode } from "vasille-js";


const s = 40;

export class App extends AppNode {
    blocks : BlockPack;


    constructor () {
        super(document.querySelector("body"), {debug: true});
        this.blocks = new BlockPack(0, 0, 26 * s, 26 * s);
    }

    createLevel (data : number[][]) {
        for (let i in data) {
            for (let j in data[i]) {
                let v = data[i][j];

                if (v !== 0) {
                    if (v === 1) {
                        this.blocks.els.add(new BrickBlock(j * s, i * s));
                    }
                    else if (v === 2) {
                        this.blocks.els.add(new BetonBlock(j * s, i * s));
                    }
                }
            }
        }

        let fastFind : { [number] : { [number] : [BlockPack] } } = { 0: {}, 1: {}, 2: {}, 3: {} };
        let ls = 8 * s;

        for (let x = 0; x < 4; x++) {
            for (let y = 0; y < 4; y++) {
                let blockPack = new BlockPack( x * ls, y * ls, ls, ls);
                this.blocks.sub.add(blockPack);
                fastFind[x][y] = blockPack;
            }
        }

        for (let block2 of this.blocks.els) {
            let block = block2.$;

            fastFind
                [Math.floor(block.x.$ / ls)]
                [Math.floor(block.y.$ / ls)]
            .els.add(block);
        }

        for (let sub of this.blocks.sub) {
            if (sub.$.els.size === 0) {
                this.blocks.sub.delete(sub);
            }
        }

        ls = 2 * s;

        for (let pack2 of this.blocks.sub) {
            let pack = pack2.$;

            for (let x = 0; x < 4; x++) {
                for (let y = 0; y < 4; y++) {
                    let blockPack = new BlockPack(pack.x.$ + x * ls, pack.y.$ + y * ls, ls, ls);
                    pack.sub.add(blockPack);
                    fastFind[x][y] = blockPack;
                }
            }

            for (let block2 of pack.els) {
                let block = block2.$;

                fastFind
                    [Math.floor((block.x.$ - pack.x.$) / ls)]
                    [Math.floor((block.y.$ - pack.y.$) / ls)]
                .els.add(block);
            }

            for (let sub of pack.sub) {
                if (sub.$.els.size === 0) {
                    pack.sub.delete(sub);
                }
            }
        }
    }



    $createDom () {
        super.$createDom();

        this.$defElement(
            new MapX1,
            $ => {
                $.width = 1040;
                $.height = 1040;
                $.els = this.blocks.els;
                $.sub = this.blocks.sub;
            });
    }
}
