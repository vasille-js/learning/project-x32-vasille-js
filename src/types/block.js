import { Reference, SetModel } from "vasille-js";
import type { IValue }         from "vasille-js";



export class Block {
    x : IValue<number>;
    y : IValue<number>;

    constructor (x : number, y : number) {
        this.x = new Reference(x);
        this.y = new Reference(y);
    }
}

export class BrickBlock extends Block {
    constructor (x : number, y : number) {
        super(x, y);
    }
}

export class BetonBlock extends Block {
    constructor (x : number, y : number) {
        super(x, y);
    }
}


export class BlockPack {
    x : IValue<number>;
    y : IValue<number>;
    width : IValue<number>;
    height: IValue<number>;
    els: SetModel<Block>;
    sub: SetModel<BlockPack>;

    constructor (x : number, y : number, width : number, height : number) {
        this.x = new Reference(x);
        this.y = new Reference(y);
        this.width = new Reference(width);
        this.height = new Reference(height);
        this.els = new SetModel();
        this.sub = new SetModel();
    }
}
