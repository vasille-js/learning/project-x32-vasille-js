import { BaseBrick } from "./BaseBrick";



export class BetonBrick extends BaseBrick {

    constructor () {
        super();
    }

    $createDom () {
        super.$createDom();

        this.className.$ = "beton-brick";
    }
}
