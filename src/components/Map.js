import { Block, BlockPack } from "../types/block";
import type { IValue } from "vasille-js";
import { ExtensionNode, SetModel, UserNode } from "vasille-js";


export class MapV1 extends UserNode {
    x : IValue<number>;
    y : IValue<number>;
    width : IValue<number>;
    height: IValue<number>;
    els: SetModel<Block>;
    sub: SetModel<BlockPack>;

    tx: IValue<number>;
    ty: IValue<number>;
    scale: IValue<number>;
    cw: IValue<number>;
    ch: IValue<number>;

    stepScale: IValue<number>;

    constructor () {
        super();

        this.x = this.$public(Number, 0);
        this.y = this.$public(Number, 0);
        this.width = this.$public(Number, 0);
        this.height = this.$public(Number, 0);
        this.els = this.$public(SetModel);
        this.sub = this.$public(SetModel);

        this.tx = this.$public(Number, 0);
        this.ty = this.$public(Number, 0);
        this.scale = this.$public(Number, 1);
        this.cw = this.$public(Number, 0);
        this.ch = this.$public(Number, 0);

        this.stepScale = this.$public(Number, 1);
    }
}
