import { BaseBrick } from "./BaseBrick";



export class BlockBrick extends BaseBrick {
    constructor () {
        super();
    }

    $created () {
        super.$created();

        this.className.$ = "block-brick";
    }
}
