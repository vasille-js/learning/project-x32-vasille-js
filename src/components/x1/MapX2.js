import { BlockPack } from "../../types/block";
import { IValue, SetView } from "vasille-js";
import { MapV1 } from "../Map";
import { MapX4 } from "./MapX4";


export class MapX2 extends MapV1 {

    lWidth: IValue<number>;
    lHeight: IValue<number>;
    lScale: IValue<number>;
    left: IValue<number>;
    right: IValue<number>;
    top: IValue<number>;
    bottom: IValue<number>;
    hVisible: IValue<boolean>;
    vVisible: IValue<boolean>;
    visible: IValue<boolean>;

    $created () {
        super.$created();

        this.lWidth = this.$bind((w, step) => w * step, this.width, this.stepScale);
        this.lHeight = this.$bind((h, step) => h * step, this.height, this.stepScale);
        this.lScale = this.$bind((scale, step) => scale / step, this.scale, this.stepScale);
        this.left = this.$bind((x, scale, tx) => x * scale + tx, this.x, this.scale, this.tx);
        this.right = this.$bind((left, w, scale) => left + w * scale, this.left, this.lWidth, this.lScale);
        this.top = this.$bind((y, scale, ty) => y * scale + ty, this.y, this.scale, this.ty);
        this.bottom = this.$bind((top, h, scale) => top + h * scale, this.top, this.lHeight, this.lScale);
        this.hVisible = this.$bind((left, right, cw) => left < cw && right > 0, this.left, this.right, this.cw);
        this.vVisible = this.$bind((top, bottom, ch) => top < ch && bottom > 0, this.top, this.bottom, this.ch);
        this.visible = this.$bind((h, v) => h && v, this.hVisible, this.vVisible);
    }

    $createDom () {
        super.$createDom();

        this.$defTag(
            "div",
            div => {
                div.$addClasses("map", "l1");
                div.$defStyles(
                    {
                        "width"     : div.$bind((w, s) => (s === 2 ? w : 0) + "px", this.lWidth, this.stepScale),
                        "height"    : div.$bind((h, s) => (s === 2 ? h : 0) + "px", this.lHeight, this.stepScale),
                        "transform" : div.$bind(
                            (left, top, scale, stepScale, v) => {
                                if (stepScale === 2 && v) {
                                    return "translate(" + left + "px, " + top + "px) scale(" + scale + ")";
                                }
                                else {
                                    return "";
                                }
                            },
                            this.left, this.top, this.lScale, this.stepScale, this.visible
                        ),
                        "position"  : div.$bind(step => step === 2 ? "absolute" : "static", this.stepScale),
                        "will-change": div.$bind((v, step) => step === 2 && v ? "transform" : "", this.visible, this.stepScale),
                        "display": div.$bind((v, step) => v || (step !== 2) ? '' : 'none', this.visible, this.stepScale)
                    });


                div.$defRepeater(
                    new SetView,
                    $ => {
                        $.model = this.sub;
                    }, (node, item : BlockPack) => {
                        node.$defElement(
                            new MapX4,
                            $ => {
                                $.x = $.$bind(x => x, item.x);
                                $.y = $.$bind(y => y, item.y);
                                $.width = $.$bind(w => w, item.width);
                                $.height = $.$bind(h => h, item.height);
                                $.els = item.els;
                                $.sub = item.sub;
                                $.tx = $.$bind(tx => tx, this.tx);
                                $.ty = $.$bind(ty => ty, this.ty);
                                $.scale = $.$bind(s => s, this.scale);
                                $.cw = this.cw;
                                $.ch = this.ch;
                                $.stepScale = $.$bind(s => s, this.stepScale);
                                $.l1x = this.x;
                                $.l1y = this.y;
                            },
                            x4 => {
                                x4.$bindShow(this.visible);
                            }
                        );
                    }
                );
            }
        );
    }
}
