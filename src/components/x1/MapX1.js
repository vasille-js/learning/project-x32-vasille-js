import { BlockPack } from "../../types/block";
import { SetView } from "vasille-js";
import { MapV1 } from "../Map";
import { MapX2 } from "./MapX2";


export class MapX1 extends MapV1 {

    rtx: number;
    rty: number
    rscale : number;
    btx: number;
    bty: number;
    bscale: number;
    beginTime: number;
    endTime: number;
    isAnim: boolean;

    constructor () {
        super();

        this.stepScale = this.$bind(
            scale => {
                if (scale <= 1) {
                    return 1;
                }
                if (scale <= 2) {
                    return 2;
                }
                if (scale <= 4) {
                    return 4;
                }
                if (scale <= 8) {
                    return 8;
                }
                if (scale <= 16) {
                    return 16;
                }
                if (scale <= 32) {
                    return 32;
                }
                return scale;
            },
            this.scale
        );
    }

    $created () {
        super.$created();

        document.onmousedown = press => {
            let tx = this.tx.$;
            let ty = this.ty.$;

            press.preventDefault();

            document.onmousemove = move => {
                this.tx.$ = this.rtx = tx + (move.screenX - press.screenX);
                this.ty.$ = this.rty = ty + (move.screenY - press.screenY);

                move.preventDefault();
            }

            document.onmouseup = up => {
                document.onmousemove = null;
                document.onmouseup = null;
                up.preventDefault();
            }
        }

        document.onwheel = wheel => {
            let step = this.stepScale.$;
            let cscale = this.rscale || this.scale.$;
            let ctx = this.rtx || this.tx.$;
            let cty = this.rty || this.ty.$;
            let nscale;

            if (wheel.deltaY < 0) {
                let v = cscale + 0.1 * step;

                if (v < 32) {
                    nscale = v;
                }
                else {
                    nscale = 32;
                }
            }
            else {
                let v = cscale - 0.1 * step;

                if (v > 0.5) {
                    nscale = v;
                }
                else {
                    nscale = 0.5;
                }
            }

            this.animTo(
                ctx - (nscale - cscale) * (wheel.clientX - ctx) / cscale,
                cty - (nscale - cscale) * (wheel.clientY - cty) / cscale,
                nscale
            );
        }

        window.onkeypress = () => {
            document.onwheel({deltaY: -1, clientX: 0, clientY: 0});
        }

        window.onresize = () => {
            this.cw.$ = window.innerWidth;
            this.ch.$ = window.innerHeight;
        }

        window.onresize();
    }

    animTo (x, y, scale) {
        this.rtx = x;
        this.rty = y;
        this.rscale = scale;
        this.btx = this.tx.$;
        this.bty = this.ty.$;
        this.bscale = this.scale.$;

        this.beginTime = new Date().getTime();
        this.endTime = this.beginTime + 300;

        if (!this.isAnim) {
            this.isAnim = true;

            let update = () => {
                let now = new Date().getTime();

                if (now > this.endTime) {
                    this.isAnim = false;
                    this.tx.$ = this.rtx;
                    this.ty.$ = this.rty;
                    this.scale.$ = this.rscale;
                }
                else {
                    let progress = (now - this.beginTime) / 300;
                    let step = Math.cos(Math.PI * progress) - 1;

                    this.tx.$ = -(this.rtx - this.btx) / 2 * step + this.btx;
                    this.ty.$ = -(this.rty - this.bty) / 2 * step + this.bty;
                    this.scale.$ = -(this.rscale - this.bscale) / 2 * step + this.bscale;

                    window.requestAnimationFrame(update);
                }
            };

            window.requestAnimationFrame(update);
        }
    }

    $createDom () {
        super.$createDom();

        this.$defTag(
            "div",
            div => {
                div.$addClass("map");
                div.$bindClass(null, this.$bind(stepScale => "x" + stepScale, this.stepScale));
                div.$defStyles(
                    {
                        "width"     : div.$bind((w, s) => (s === 2 ? w : 0) + "px", this.width, this.stepScale),
                        "height"    : div.$bind((h, s) => (s === 2 ? h : 0) + "px", this.height, this.stepScale),
                        "transform" : div.$bind(
                            (x, tx, y, ty, scale, stepScale) => {
                                if (stepScale === 1) {
                                    return "translate(" + (x + tx) + "px, " + (y + ty) + "px) scale(" + scale + ")";
                                }
                                else {
                                    return "";
                                }
                            },
                            this.x, this.tx, this.y, this.ty, this.scale, this.stepScale
                        ),
                        "position"  : div.$bind(step => step === 1 ? "absolute" : "static", this.stepScale),
                        "will-change": div.$bind(step => step === 1 ? "transform" : "", this.stepScale)
                    });

                div.$defRepeater(
                    new SetView,
                    $ => {
                        $.model = this.sub;
                    }, (node, item : BlockPack) => {
                        node.$defElement(
                            new MapX2,
                            $ => {
                                $.x = item.x;
                                $.y = item.y;
                                $.width = item.width;
                                $.height = item.height;
                                $.els = item.els;
                                $.sub = item.sub;
                                $.tx = this.tx;
                                $.ty = this.ty;
                                $.scale = this.scale;
                                $.cw = this.cw;
                                $.ch = this.ch;
                                $.stepScale = this.stepScale;
                            }
                        );
                    }
                );
            }
        );
    }
}
