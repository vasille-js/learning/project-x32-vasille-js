import { BetonBlock, Block, BrickBlock } from "../../types/block";
import { IValue, SetView } from "vasille-js";
import { BetonBrick } from "../BetonBrick";
import { BlockBrick } from "../BlockBrick";
import { MapV1 } from "../Map";


export class MapX4 extends MapV1 {

    l1x : IValue<number>;
    l1y : IValue<number>;

    lWidth: IValue<number>;
    lHeight: IValue<number>;
    lScale: IValue<number>;
    left: IValue<number>;
    right: IValue<number>;
    top: IValue<number>;
    bottom: IValue<number>;
    hVisible: IValue<boolean>;
    vVisible: IValue<boolean>;
    visible: IValue<boolean>;

    static i = 1;

    constructor (props) {
        super(props);

        this.l1x = this.$public(Number, 0);
        this.l1y = this.$public(Number, 0);
    }

    $created () {
        super.$created();

        this.lWidth = this.$bind((w, step) => w * step, this.width, this.stepScale);
        this.lHeight = this.$bind((h, step) => h * step, this.height, this.stepScale);
        this.lScale = this.$bind((scale, step) => scale / step, this.scale, this.stepScale);
        this.left = this.$bind((x, scale, tx) => x * scale + tx, this.x, this.scale, this.tx);
        this.right = this.$bind((left, w, scale) => left + w * scale, this.left, this.lWidth, this.lScale);
        this.top = this.$bind((y, scale, ty) => y * scale + ty, this.y, this.scale, this.ty);
        this.bottom = this.$bind((top, h, scale) => top + h * scale, this.top, this.lHeight, this.lScale);
        this.hVisible = this.$bind((left, right, cw) => left < cw && right > 0, this.left, this.right, this.cw);
        this.vVisible = this.$bind((top, bottom, ch) => top < ch && bottom > 0, this.top, this.bottom, this.ch);
        this.visible = this.$bind((h, v) => h && v, this.hVisible, this.vVisible);
    }

    $createDom () {
        super.$createDom();

        this.$defTag(
            "div",
            div => {
                div.$addClasses("map", "l2", 'i' + ++MapX4.i);
                div.$defStyles(
                    {
                        "width"     : div.$bind((w, s) => (s === 4 ? w : 0) + "px", this.lWidth, this.stepScale),
                        "height"    : div.$bind((h, s) => (s === 4 ? h : 0) + "px", this.lHeight, this.stepScale),
                        "transform" : div.$bind(
                            (top, left, scale, stepScale, v) => {
                                if (stepScale === 4 && v) {
                                    return "translate(" + left + "px, " + top + "px) scale(" + scale + ")";
                                }
                                else {
                                    return "";
                                }
                            },
                            this.top, this.left, this.lScale, this.stepScale, this.visible
                        ),
                        "position"  : div.$bind(step => step === 4 ? "absolute" : "static", this.stepScale),
                        "will-change": div.$bind((v, step) => step === 4 && v ? "transform" : "", this.visible, this.stepScale),
                        "display": div.$bind((v, step) => v || (step !== 4) ? 'block' : 'none', this.visible, this.stepScale)
                    }
                );

                let props = ($ : BaseBrick, item : Block) => {
                    $.x = item.x;
                    $.y = item.y;
                    $.tx = $.$bind(tx => tx, this.tx);
                    $.ty = $.$bind(ty => ty, this.ty);
                    $.l1x = this.l1x;
                    $.l1y = this.l1y;
                    $.l2x = this.x;
                    $.l2y = this.y;
                    $.step = $.$bind(s => s, this.stepScale);
                    $.scale = $.$bind(s => s, this.scale);
                    $.cw = this.cw;
                    $.ch = this.ch;
                }

                div.$defRepeater(
                    new SetView,
                    $ => {
                        $.model = this.els;
                    },
                    (node, item : Block) => {
                        if (item instanceof BrickBlock) {
                            node.$defElement(new BlockBrick, $ => props($, item), brick => {
                                // brick.$bindShow(this.visible);
                            });
                        }
                        if (item instanceof BetonBlock) {
                            node.$defElement(new BetonBrick, $ => props($, item), brick => {
                                // brick.$bindShow(this.visible);
                            });
                        }
                    }
                );
            }
        );
    }
}
