import { ExtensionNode, IValue } from "vasille-js";



export class BaseBrick extends ExtensionNode {
    x : IValue<number>;
    y : IValue<number>;
    tx : IValue<number>;
    ty : IValue<number>;
    l1x : IValue<number>;
    l1y : IValue<number>;
    l2x : IValue<number>;
    l2y : IValue<number>;
    step : IValue<number>;
    scale : IValue<number>;
    className : IValue<string>;
    cw : IValue<number>;
    ch : IValue<number>;

    cx : IValue<number>;
    cy : IValue<number>;
    lscale: IValue<number>;

    lWidth: IValue<number>;
    lHeight: IValue<number>;
    left: IValue<number>;
    right: IValue<number>;
    top: IValue<number>;
    bottom: IValue<number>;
    hVisible: IValue<boolean>;
    vVisible: IValue<boolean>;
    visible: IValue<boolean>;

    constructor () {
        super();

        this.x = this.$public(Number, 0);
        this.y = this.$public(Number, 0);
        this.tx = this.$public(Number, 0);
        this.ty = this.$public(Number, 0);
        this.l1x = this.$public(Number, 0);
        this.l2x = this.$public(Number, 0);
        this.l1y = this.$public(Number, 0);
        this.l2y = this.$public(Number, 0);
        this.step = this.$public(Number, 0);
        this.scale = this.$public(Number, 0);
        this.className = this.$public(String, null);
        this.cw = this.$public(Number, 0);
        this.ch = this.$public(Number, 0);
    }

    $created () {
        super.$created();

        this.cx = this.$bind(
            (x, l1x, l2x, step) =>
                step < 2 || step > 4
                ? x * step
                : step === 2
                  ? 2 * (x - l1x)
                  : 4 * (x - l2x),
            this.x, this.l1x, this.l2x, this.step
        );

        this.cy = this.$bind(
            (y, l1y, l2y, step) =>
                step < 2 || step > 4
                ? y * step
                : step === 2
                  ? 2 * (y - l1y)
                  : 4 * (y - l2y),
            this.y, this.l1y, this.l2y, this.step
        );

        this.lscale = this.$bind((scale, step) => scale / step, this.scale, this.step);

        this.lWidth = this.$bind(step => 40 * step, this.step);
        this.lHeight = this.$bind(step => 40 * step, this.step);
        this.left = this.$bind((x, scale, tx) => x * scale + tx, this.x, this.scale, this.tx);
        this.right = this.$bind((left, w, scale) => left + w * scale, this.left, this.lWidth, this.lscale);
        this.top = this.$bind((y, scale, ty) => y * scale + ty, this.y, this.scale, this.ty);
        this.bottom = this.$bind((top, h, scale) => top + h * scale, this.top, this.lHeight, this.lscale);
        this.hVisible = this.$bind((left, right, cw) => left < cw && right > 0, this.left, this.right, this.cw);
        this.vVisible = this.$bind((top, bottom, ch) => top < ch && bottom > 0, this.top, this.bottom, this.ch);
        this.visible = this.$bind((h, v, step) => (h && v && step > 4) || step <= 4, this.hVisible, this.vVisible, this.step);
    }

    $createDom () {
        super.$createDom();

        this.$defTag("div", div => {
            div.$addClass("block");
            div.$bindClass(null, this.className);
            div.$defStyles(
                {
                    "transform" : div.$bind(
                        (x, left, y, top, scale, step, v) => {
                            return !v ? "" :
                                   step <= 4
                                   ? "translate(" + x + "px, " + y + "px)"
                                   : "translate(" + left + "px, " + top + "px) scale(" + scale + ")";
                        },
                        this.cx, this.left, this.cy, this.top, this.lscale, this.step, this.visible),
                    "will-change": div.$bind(step => step > 4 ? "transform" : "", this.step),
                    "display": div.$bind(v => v ? '' : 'none', this.visible)
                });
        });
    }
}
