const path = require ("path");

module.exports = {
    mode : "development",
    entry : "./src/main.js",
    output : {
        filename : "app.js",
        path : path.resolve (__dirname, "dist")
    },
    module : {
        rules : [
            {
                test : /\.js$/,
                use : "babel-loader"
            }
        ]
    },
    stats : {
        colors : true
    },
    devtool : "source-map",
    optimization : {
        minimize : false
    },
    watch : false,
    watchOptions : {
        ignored : /node_modules/,
        aggregateTimeout : 10000
    },
    devServer : {
        contentBase : path.join (__dirname, 'dist'),
        port : 3000
    }
};
